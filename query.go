package main

import (
	"fmt"
	"os/exec"

	"gitlab.com/bluebottle/go-modules/misc"
)

// OriginList Output the originating repository of given package
func OriginList(pack string) {
	//
	// TODO: Rebuild with use of https://github.com/Sabayon/pkgs-checker ?
	//
	out, err := exec.Command("bash", "-c", fmt.Sprintf("VAR=`equery m %s|grep Location|awk '{split($0,a,\":\"); printf \"%%s/metadata.xml\", a[2]}'`; if test -z \"$VAR\"; then echo ''; else cat $VAR|grep origin|cut -d'>' -f2|cut -d'<' -f1; fi", pack)).CombinedOutput()
	misc.ShowError(err, "", "ErrFatal")
	fmt.Printf("%s\n", out)
} // OriginList ()
