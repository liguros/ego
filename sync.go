package main

import (
	"fmt"
	"os"
	"path/filepath"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"golang.org/x/sys/unix"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/ego/colors"
	"gitlab.com/liguros/ego/configuration"
)

// Return public ssh key
func publicKey() *ssh.PublicKeys {
	sshPath := filepath.Clean(os.Getenv("HOME") + "/.ssh/id_rsa")
	sshKey, err := os.ReadFile(sshPath)
	misc.ShowError(err, "", "ErrPanic")
	publicKey, err := ssh.NewPublicKeys("git", []byte(sshKey), "")
	misc.ShowError(err, "", "ErrPanic")
	return publicKey
} // publicKey ()

// Clone the desired repo with given name and URL. If needed a ssh key will be used.
func cloneRepo(path, url string, reference plumbing.ReferenceName, sshNeeded bool) (err error) {
	if sshNeeded {
		_, err = git.PlainClone(path, false, &git.CloneOptions{
			URL:           url,
			ReferenceName: "refs/heads/" + reference,
			Progress:      os.Stdout,
			Auth:          publicKey(),
		})
	} else {
		//
		// TODO: Check error message with no authentication for git URL
		//
		_, err = git.PlainClone(path, false, &git.CloneOptions{
			URL:           url,
			ReferenceName: "refs/heads/" + reference,
			Progress:      os.Stdout,
		})
	} // if

	return
} // cloneRepo ()

// Pull updates for the desired repo with given name and URL. If needed a ssh key will be used.
func updateRepo(path, url string, reference plumbing.ReferenceName, sshNeeded bool) (err error) {
	var (
		tmpRepo *git.Repository
		tmpTree *git.Worktree
	)

	tmpRepo, err = git.PlainOpen(path)
	misc.ShowError(err, "", "ErrPanic")
	tmpTree, err = tmpRepo.Worktree()
	misc.ShowError(err, "", "ErrPanic")
	fmt.Println("Fetching changes from remote")

	if sshNeeded {
		err = tmpRepo.Fetch(&git.FetchOptions{
			RemoteName: "origin",
			RefSpecs: []config.RefSpec{
				"+refs/heads/*:refs/remotes/origin/*",
				"refs/heads/*:refs/heads/*",
			},
			Force:    true,
			Progress: os.Stdout,
			Auth:     publicKey(),
		})
	} else {
		err = tmpRepo.Fetch(&git.FetchOptions{
			RemoteName: "origin",
			RefSpecs: []config.RefSpec{
				"+refs/heads/*:refs/remotes/origin/*",
				"refs/heads/*:refs/heads/*",
			},
			Force:    true,
			Progress: os.Stdout,
		})
	} // if

	misc.ShowError(err, "", "ErrPrint")
	fmt.Println("Checking out", reference, "branch.")
	err = tmpTree.Checkout(&git.CheckoutOptions{
		Branch: "refs/heads/" + reference,
		Force:  true,
	})

	return
} // updateRepo ()

// Detemines the status of the working tree and returns it. If the status map contains elements, the tree is not clean.
func statusRepo(path string) (git.Status, error) {
	var (
		err     error
		tmpRepo *git.Repository
		tmpTree *git.Worktree
	)

	tmpRepo, err = git.PlainOpen(path)
	misc.ShowError(err, "", "ErrPanic")
	tmpTree, err = tmpRepo.Worktree()
	misc.ShowError(err, "", "ErrPanic")
	return tmpTree.Status()
} // statusRepo ()

// Segundo handles the flag and subcommand coordination.
func Segundo() error {
	// Check which of the possible flags are set
	metaRepoPath := configuration.Configuration.Location
	reposConfPath := configuration.Configuration.ReposConfPath

	// check if meta root path is not writable
	checkMetaRepoPath(metaRepoPath)

	// Looping through repos.conf directory and update repositories
	err := filepath.Walk(reposConfPath, func(path string, f os.FileInfo, err error) error {
		var treeStatus git.Status

		if f != nil {
			syncAction(f, path, treeStatus)
		} // if

		return err
	})

	return err
} // Segundo

// func autoSync(treeStatus git.Status) (err error) {
func autoSync() (err error) {
	var treeStatus git.Status
	err = cloneRepo(configuration.Configuration.Location, configuration.Configuration.SyncURI, plumbing.ReferenceName(configuration.Configuration.Release), false)

	// Check if the error says that the repo already exists => pull updates instead
	if err == git.ErrRepositoryAlreadyExists {
		fmt.Println("Checking local tree status: " + configuration.Configuration.Location)
		// check tree status before updating to avoid errors due to uncommited or unstaged changes
		treeStatus, err = statusRepo(configuration.Configuration.Location)
		misc.ShowError(err, "", "ErrFatal")

		// check if we have an unclean tree. If yes then print a message and exit.
		if len(treeStatus) > 0 {
			misc.ShowError(nil, "Can not update tree "+configuration.Configuration.Location+" as there are open changes.\nThese are the corresponding files:\n"+treeStatus.String(), "MsgFatal")
		} // if

		fmt.Println("Trying to pull changes from remote: " + configuration.Configuration.SyncURI)
		err = updateRepo(configuration.Configuration.Location, configuration.Configuration.SyncURI, plumbing.ReferenceName(configuration.Configuration.Release), false)

		// Check for common errors to give message and reset error as we handle them separately
		switch err {
		case git.NoErrAlreadyUpToDate:
			fmt.Println("Already up-to-date.")
			err = nil
		case git.ErrUnstagedChanges:
			misc.ShowError(nil, configuration.Configuration.Location+" contains unstaged changes.", "MsgFatal")
		case git.ErrNonFastForwardUpdate:
			misc.ShowError(nil, configuration.Configuration.Location+" non-fast-forward update.", "MsgFatal")
		} // switch
	} // if

	return err
} // autoSync ()

func checkMetaRepoPath(metaRepoPath string) {
	info, err := os.Stat(metaRepoPath)

	// Check if meta repo path exists and is a file
	if os.IsNotExist(err) {
		// It does not exist so create it
		if err := os.MkdirAll(metaRepoPath, os.ModePerm|os.ModeDir); err != nil {
			misc.ShowError(err, "", "ErrFatal")
		} // if
	} else if !info.IsDir() {
		misc.ShowError(nil, metaRepoPath+" exists and is not a directory", "MsgFatal")
	} // if

	if unix.Access(filepath.Dir(metaRepoPath), unix.W_OK) != nil {
		misc.ShowError(nil, fmt.Sprintf("Meta repo path '%s' either doesn't exist or is not writable", metaRepoPath), "MsgFatal")
	} // if
} // checkMetaRepoPath

func syncAction(f os.FileInfo, path string, treeStatus git.Status) {
	if !f.IsDir() {
		fmt.Println(fmt.Sprintf(colors.Bold, "Updating repository "+f.Name()))
		// Read ini file of repository
		configuration.ReadReposConfig(path)

		if configuration.Configuration.AutoSync == "yes" {
			misc.ShowError(autoSync(), "", "ErrFatal")
		} else {
			fmt.Println("Autosync disabled")
		} // if
	} // if
} // syncAction ()
