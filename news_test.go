package main

import (
	"testing"
)

// Testing parseData function.
func TestParseDate(t *testing.T) {
	tables := []struct {
		s string
		t string
	}{
		{"", "0001-01-01 00:00:00 +0000 UTC"},
		{"2020-01-01", "0001-01-01 00:00:00 +0000 UTC"},
		{"2019-12-30T15:29:52.009Z", "2019-12-30 15:29:52.009 +0000 UTC"},
	}

	for _, table := range tables {
		total, _ := parseDate(table.s)
		totalStr := total.String()

		if totalStr != table.t {
			t.Errorf("Parsed date is incorrect, got: %s, want: %s.", totalStr, table.t)
		} // if
	} // for
} // TestParseDate ()
