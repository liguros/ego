// Package configuration handles the configuration of the program.
package configuration

import (
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/v2"

	"gitlab.com/bluebottle/go-modules/misc"

	"gopkg.in/ini.v1"
)

// Config the configuration structure
type Config struct {
	Location      string
	Priority      string
	AutoSync      string
	SyncType      string
	SyncURI       string
	MainRepo      string
	Release       string
	ReposConfPath string
}

// Configuration contains the current configuration values
var Configuration Config

// ReadProgConfig Reads configuration file and fills configuration values.
func ReadProgConfig(configfile string) {
	v := koanf.New(".")
	err := v.Load(file.Provider(configfile), yaml.Parser())
	misc.ShowError(err, "", "ErrPanic")
	// Add read config values to the configuration variable
	Configuration.Release = v.String("global.release")
	Configuration.ReposConfPath = v.String("global.reposconfpath")
	// Read the given ini file
	ReadReposConfig(v.String("global.reposconfpath"))
} // readProgConfig ()

// ReadReposConfig Reads repos configuration file and updates config values.
func ReadReposConfig(filename string) {
	cfg, err := ini.Load(filename)
	misc.ShowError(err, "", "ErrPanic")
	Configuration.MainRepo = cfg.Section("DEFAULT").Key("main-repo").String()
	Configuration.Location = cfg.Section("liguros-repo").Key("location").String()
	Configuration.AutoSync = cfg.Section("liguros-repo").Key("auto-sync").String()
	Configuration.SyncType = cfg.Section("liguros-repo").Key("sync-type").String()
	Configuration.SyncURI = cfg.Section("liguros-repo").Key("sync-uri").String()
	Configuration.Priority = cfg.Section("liguros-repo").Key("priority").String()
} // readReposConfig ()
