package main

import (
	"fmt"
	"testing"

	"github.com/amalfra/recorder"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/ego/colors"
)

// Testing parseData function.
func TestPrintInheritanceResult(t *testing.T) {
	tables := []struct {
		sa []string
		s  string
		t  string
	}{
		{[]string{}, "", ""},
		{[]string{"Test1"}, "TestHeader", fmt.Sprintf("\n=== "+colors.Green+" ===\n\n", "TestHeader") + fmt.Sprintf(colors.BoldRightSize+"\n", 50, "Test1")},
		{[]string{"Test1", "Test2"}, "TestHeader", fmt.Sprintf("\n=== "+colors.Green+" ===\n\n", "TestHeader") + fmt.Sprintf(colors.BoldRightSize+"\n", 50, "Test1") + fmt.Sprintf(colors.BoldRightSize+"\n", 50, "Test2")},
	}

	for _, table := range tables {
		rec := new(recorder.Stdout)
		err := rec.Start()
		misc.ShowError(err, "", "ErrPrint")
		printInheritanceResult(table.sa, table.s)
		rec.Stop()
		totalStr := rec.GetOutput()

		if totalStr != table.t {
			t.Errorf("Result is incorrect, got: %s, want: %s.", totalStr, table.t)
		} // if
	} // for
} // TestPrintInheritanceResult ()
