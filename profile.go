package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/eidolon/wordwrap"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/ego/configuration"
)

type parentProfile map[string]interface{}

// Profile prefix
const prefix = "profiles/liguros/1.0/linux-gnu"

// Read standard parent file and return structure with current values for arch, subarch, build, flavor and mix-ins.
func gatherProfileValues(fPath string) (parPro parentProfile) {
	// read /etc/portage/make.profile/parent and display current values
	file, err := os.Open(filepath.Clean(fPath))
	misc.ShowError(err, "", "ErrPanic")
	scanner := bufio.NewScanner(file)
	// Create parentProfile and set default values
	parPro = make(map[string]interface{})
	parPro["arch"] = ""
	parPro["subarch"] = ""
	parPro["build"] = ""
	parPro["flavor"] = ""
	parPro["mix-ins"] = []string{}

	// split input lines and update profile according to the line values
	for scanner.Scan() {
		setParProfValues(strings.Split(scanner.Text(), "/"), &parPro)
	} // for

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	} // if

	misc.ShowError(file.Close(), "", "ErrPanic")
	return
} // gatherProfileValues ()

func setParProfValues(tmpStr []string, parPro *parentProfile) {
	tmpIndex := 4
	tmpMap := tmpStr[3]

	// handle special cases (subarch => pos 5 if pos 3 = arch), else type = pos 3 and value = pos 4
	if tmpMap == "arch" && len(tmpStr) > 5 && tmpStr[5] == "subarch" {
		tmpIndex = 6
		tmpMap = "subarch"
	} // if

	if tmpMap == "mix-ins" {
		(*parPro)[tmpMap] = append((*parPro)[tmpMap].([]string), tmpStr[tmpIndex])
	} else {
		(*parPro)[tmpMap] = tmpStr[tmpIndex]
	} // if
} // setParProfValues ()

// Write parent file with new values for arch, subarch, build, flavor and mix-ins.
func writeProfileValues(parPro parentProfile, fPath string) error {
	//
	// TODO: Make backup of old parent file only if desired?
	//
	// Make a backup of the file first
	misc.ShowError(os.Rename(fPath, fPath+".bak"), "", "ErrFatal")

	// write new parent file
	file, err := os.Create(filepath.Clean(fPath))
	misc.ShowError(err, "", "ErrPanic")

	tmpPref := strings.Join(strings.Split(prefix, "/")[1:], "/")
	tmpRepo := configuration.Configuration.MainRepo

	tmpArch := tmpRepo + ":" + tmpPref + "/arch/" + parPro["arch"].(string)
	// Writing arch, subarch, build and flavor values
	_, err = fmt.Fprintln(file, tmpArch+"\n"+
		tmpArch+"/subarch/"+parPro["subarch"].(string)+"\n"+
		tmpRepo+":"+tmpPref+"/build/"+parPro["build"].(string)+"\n"+
		tmpRepo+":"+tmpPref+"/flavor/"+parPro["flavor"].(string))
	misc.ShowError(err, "", "ErrPanic")

	// Write mix-ins
	for _, v := range parPro["mix-ins"].([]string) {
		_, err = fmt.Fprintln(file, tmpRepo+":"+tmpPref+"/mix-ins/"+v)
		misc.ShowError(err, "", "ErrPanic")
	} // for

	return file.Close()
} // writeProfileValues()

// get inheritance values from a given parent file
func getInheritanceParentFile(metapath, parentFile string) (inherited []string) {
	// Check if file exists
	parentDir := path.Dir(parentFile)
	_, err := os.Stat(parentFile)

	if !os.IsNotExist(err) {
		file, err := os.Open(filepath.Clean(parentFile))
		misc.ShowError(err, "", "ErrPanic")
		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)

		for scanner.Scan() {
			scanInheritedPath(parentDir, metapath, scanner, &inherited)
		} // for

		err = file.Close()
		misc.ShowError(err, "", "ErrPanic")
	} // if

	return
} // getInheritanceParentFile ()

func printInheritanceResult(strArr []string, header string) {
	if len(strArr) > 0 {
		fmt.Println(fmt.Sprintf("\n=== "+misc.Green+" ===\n", header))

		for _, v := range strArr {
			fmt.Println(fmt.Sprintf(misc.BoldRightSize, 50, v))
		} // for
	} // if
} // printInheritanceResult ()

func defaultHandling(metaPath string, proValues parentProfile) {
	// Display gathered profile values
	fmt.Println(fmt.Sprintf("\n=== "+misc.Green+": ===\n", "Enabled Profiles"))
	fmt.Println(fmt.Sprintf(misc.BoldRightSize+": "+misc.LightBlue, 12, "arch", proValues["arch"].(string)))
	fmt.Println(fmt.Sprintf(misc.BoldRightSize+": "+misc.LightBlue, 12, "subarch", proValues["subarch"].(string)))
	fmt.Println(fmt.Sprintf(misc.BoldRightSize+": "+misc.LightBlue, 12, "build", proValues["build"].(string)))
	fmt.Println(fmt.Sprintf(misc.BoldRightSize+": "+misc.LightBlue, 12, "flavor", proValues["flavor"].(string)))

	for _, v := range proValues["mix-ins"].([]string) {
		fmt.Println(fmt.Sprintf(misc.BoldRightSize+": "+misc.LightBlue, 12, "mix-ins", v))
	} // for

	//
	// TODO: display also the inherited mix-ins
	//
	// Scan flavor, arch, subarch and mix-ins dirs from profiles/liguros/1.0/linux-gnu/ for parent files
	// build list of included mix-ins and flavors, then display them
	var inheritedProfiles = make(map[string][]string)
	tmpPath := metaPath + "/" + prefix

	inheritedProfiles["Arch"] = getInheritanceParentFile(metaPath, tmpPath+"/arch/"+proValues["arch"].(string)+"/parent")
	printInheritanceResult(inheritedProfiles["Arch"], "All inherited from arch "+proValues["arch"].(string))

	inheritedProfiles["Subarch"] = getInheritanceParentFile(metaPath, tmpPath+"/arch/"+proValues["arch"].(string)+"/subarch/"+proValues["subarch"].(string)+"/parent")
	printInheritanceResult(inheritedProfiles["Subarch"], "All inherited from subarch "+proValues["subarch"].(string))

	inheritedProfiles["Build"] = getInheritanceParentFile(metaPath, tmpPath+"/build/"+proValues["build"].(string)+"/parent")
	printInheritanceResult(inheritedProfiles["Build"], "All inherited from build "+proValues["build"].(string))

	inheritedProfiles["Flavor"] = getInheritanceParentFile(metaPath, tmpPath+"/flavor/"+proValues["flavor"].(string)+"/parent")
	printInheritanceResult(inheritedProfiles["Flavor"], "All inherited from arch "+proValues["flavor"].(string))

	for _, v := range proValues["mix-ins"].([]string) {
		inheritedProfiles["Mix-in "+v] = getInheritanceParentFile(metaPath, tmpPath+"/mix-ins/"+v+"/parent")
		printInheritanceResult(inheritedProfiles["Mix-in "+v], "All inherited from mix-in "+v)
	} // for
} // defaultHandling()

// set config value to given value (including necessary checks)
// func setConfig(repPath string, parProf parentProfile, typeChg string, newVal string) {
func setConfig(repPath string, parProf parentProfile, typeChg, newVal, fPath string) {
	//
	// TODO: Not only set the value but also check if it is a valid one
	//       Especially if the given value is not empty
	//       Reading in the possible values each time?
	//
	switch typeChg {
	case "subarch", "arch", "build", "flavor":
		parProf[typeChg] = newVal
	case "mix-ins":
		if newVal != "" {
			handleMixins(newVal, &parProf)
		} // if
	} // switch

	// Writing new profile values
	err := writeProfileValues(parProf, fPath)
	misc.ShowError(err, "", "ErrFatal")
	// Show resulting profile
	defaultHandling(repPath, parProf)
} // setConfig ()

// Display desired profile values
func dispProfValues(dirname, profPart string, proValues parentProfile) {
	//
	// TODO: Mark also inherited values in output
	//
	currVal := ""
	var tmpMix = make(map[string]bool)

	// Get current value of profile key and map for mix-ins
	if profPart != "mix-ins" {
		var ok bool
		currVal, ok = proValues[profPart].(string)

		if !ok {
			misc.ShowError(nil, "Could not assert value", "MsgFatal")
		} // if
	} else {
		for _, v := range proValues["mix-ins"].([]string) {
			tmpMix[v] = true
		} // for
	} // if

	// names, err := ioutil.ReadDir(dirname)
	names, err := os.ReadDir(dirname)

	if !os.IsNotExist(err) {
		outputValues(err, profPart, currVal, names, tmpMix)
	} // if
} // dispProfValues ()

func outputValues(err error, profPart, currVal string, names []os.DirEntry, tmpMix map[string]bool) {
	misc.ShowError(err, "", "ErrFatal")
	fmt.Println(fmt.Sprintf("\n=== "+misc.Green+": ===\n", profPart))
	tmpStr := ""

	for _, name := range names {
		if name.IsDir() {
			displayProfileValue(name, currVal, tmpMix, &tmpStr)
		} // if
	} // for

	// Output string
	wrapper := wordwrap.Wrapper(70, true)
	fmt.Println(wordwrap.Indent(wrapper(tmpStr), "    ", true))
} // outputValues ()

// Profile handles the flag and subcommand coordination.
func Profile(types, args, fPath string) {
	// get meta repo path
	metaRepoPath := configuration.Configuration.Location
	// Get values from /etc/portage/make.profile/parent
	proValues := gatherProfileValues(fPath)

	switch types {
	case "list":
		profileRoot := configuration.Configuration.Location + "/" + prefix + "/"

		switch args {
		case "arch", "build", "flavor", "mix-ins":
			dispProfValues(profileRoot+args, args, proValues)
			fmt.Println()
		case "subarch":
			dispProfValues(profileRoot+"arch/"+proValues["arch"].(string)+"/subarch", "subarch", proValues)
			fmt.Println()
		default:
			defaultProfileHandling(profileRoot, proValues)
		} // switch
	case "arch", "build", "subarch", "flavor", "mix-ins":
		setConfig(metaRepoPath, proValues, types, args, fPath)
	default:
		defaultHandling(metaRepoPath, proValues)
	} // switch
} // Profile ()

// func displayProfileValue(name os.FileInfo, currVal string, tmpMix map[string]bool, tmpStr *string) {
func displayProfileValue(name os.DirEntry, currVal string, tmpMix map[string]bool, tmpStr *string) {
	// Build output string
	if (*tmpStr) != "" {
		(*tmpStr) += ", "
	} // if

	// Check if loop value is set as current value in profile and highlight it
	if name.Name() == currVal || tmpMix[name.Name()] {
		(*tmpStr) += fmt.Sprintf(misc.Bold, fmt.Sprintf(misc.LightBlue+"*", name.Name()))
	} else {
		(*tmpStr) += name.Name()
	} // if
} // displayProfileValue ()

func scanInheritedPath(parentDir, metapath string, scanner *bufio.Scanner, inherited *[]string) {
	// get value and remove prefix path
	tmpStr := strings.ReplaceAll(path.Clean(parentDir+"/"+scanner.Text()), metapath+"/"+prefix, "")

	// Remove / on the first position
	if tmpStr != "" && tmpStr[0] == '/' {
		tmpStr = tmpStr[1:]
	} // if

	if tmpStr != "" {
		(*inherited) = append((*inherited), tmpStr)
	} // if
} // scanInheritedPath ()

func defaultProfileHandling(profileRoot string, proValues parentProfile) {
	profileParts := []string{"arch", "subarch", "build", "flavor", "mix-ins"}

	for _, v := range profileParts {
		if v == "subarch" {
			dispProfValues(profileRoot+"arch/"+proValues["arch"].(string)+"/"+v, v, proValues)
		} else {
			dispProfValues(profileRoot+v, v, proValues)
		} // if

		fmt.Println()
	} // for
} // defaultProfileHandling ()

func handleMixins(newVal string, parProf *parentProfile) {
	//
	// TODO: Add or remove mix-ins with validation
	//
	for _, v := range strings.Split(newVal, " ") {
		// Remove mix-in if it is prefixed with "-" and exists, otherwise add it, if it not exists
		if v[0] == '-' {
			(*parProf)["mix-ins"] = misc.RemoveFromSlice((*parProf)["mix-ins"].([]string), v[1:])
		} else {
			(*parProf)["mix-ins"] = misc.AddToSlice((*parProf)["mix-ins"].([]string), v)
		} // if
	} // for
} // handleMixins ()
