package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/ego/colors"
)

type releases []release

type release struct {
	Name            string  `json:"name"`
	TagName         string  `json:"tag_name"`
	Description     string  `json:"description"`
	DescriptionHTML string  `json:"description_html"`
	CreatedAt       string  `json:"created_at"`
	ReleasedAt      string  `json:"released_at"`
	Author          author  `json:"author"`
	Commit          commit  `json:"commit"`
	UpcomingRelease bool    `json:"upcoming_release"`
	CommitPath      string  `json:"commit_path"`
	TagPath         string  `json:"tag_path"`
	EvidenceSHA     *string `json:"evidence_sha,omitempty"`
	Assets          assets  `json:"assets"`
	Links           links   `json:"_links"`
}

type assets struct {
	Count            int64         `json:"count"`
	Sources          []source      `json:"sources"`
	Links            []interface{} `json:"links"`
	EvidenceFilePath *string       `json:"evidence_file_path,omitempty"`
}

type source struct {
	Format string `json:"format"`
	URL    string `json:"url"`
}

type author struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	State     string `json:"state"`
	AvatarURL string `json:"avatar_url"`
	WebURL    string `json:"web_url"`
}

type commit struct {
	ID             string   `json:"id"`
	ShortID        string   `json:"short_id"`
	CreatedAt      string   `json:"created_at"`
	ParentIDS      []string `json:"parent_ids"`
	Title          string   `json:"title"`
	Message        string   `json:"message"`
	AuthorName     string   `json:"author_name"`
	AuthorEmail    string   `json:"author_email"`
	AuthoredDate   string   `json:"authored_date"`
	CommitterName  string   `json:"committer_name"`
	CommitterEmail string   `json:"committer_email"`
	CommittedDate  string   `json:"committed_date"`
}

type links struct {
}

// Read release notes from remote URL.
func readReleaseNotes() (rel releases) {
	resp, err := http.Get("https://gitlab.com/api/v4/projects/liguros%2Fkit-fixups/releases")
	misc.ShowError(err, "Couldn't open release url", "ErrMsgFatal")
	body, err := io.ReadAll(resp.Body)
	misc.ShowError(err, "", "ErrPanic")
	err = resp.Body.Close()
	misc.ShowError(err, "", "ErrPanic")
	err = json.Unmarshal(body, &rel)
	misc.ShowError(err, "Couldn't unmarshal json information", "ErrMsgFatal")
	return
} // readReleaseNotes ()

// Parse given date string and return time and error for comparison.
func parseDate(input string) (time.Time, error) {
	tmpDate, err := time.Parse("2006-01-02T15:04:05.000Z", input)
	return tmpDate, err
} // parseDate ()

// Display release info with the given index.
func displayInfo(r *release, curDate time.Time) {
	fmt.Println(fmt.Sprintf("=== "+colors.Green+" ===", r.Name))
	fmt.Println(fmt.Sprintf(colors.BoldSize+colors.Yellow, 14, "Tag:", r.TagName))
	fmt.Println(fmt.Sprintf(colors.BoldSize+colors.Yellow, 14, "Released:", r.ReleasedAt))
	releaseDate, err := parseDate(r.ReleasedAt)
	misc.ShowError(err, "Present date not correctly parsed", "ErrMsgFatal")

	if curDate.Before(releaseDate) {
		fmt.Println("Unreleased future release")
	} // if

	fmt.Println("\n  " + strings.ReplaceAll(r.Description, "\n", "\n  "))
	fmt.Println(strings.Repeat("-", 35))
} // displayInfo ()

// Releases Show given number of release notes
func Releases(number int) {
	rel := readReleaseNotes()
	presentDate, err := parseDate(time.Now().Format("2006-01-02T15:04:05.000Z"))
	misc.ShowError(err, "Present date not correctly parsed", "ErrMsgFatal")

	// Loop through JSON data.
	for i := 0; i < number; i++ {
		displayInfo(&rel[i], presentDate)
	} // for
} // Releases ()
