package colors

var (
	// Bold bold text
	Bold = "\033[1m%s\033[0m"
	// BoldSize bold text with fixed width
	BoldSize = "\033[1m%-*s\033[0m"
	// BoldRightSize bold text with fixed width right aligned
	BoldRightSize = "\033[1m%*s\033[0m"
	// Green green text
	Green = "\033[92m%s\033[0m"
	// Red red text
	Red = "\033[91m%s\033[0m"
	// Purple purple text
	Purple = "\033[95m%s\033[0m"
	// Yellow yellow text
	Yellow = "\033[96m%s\033[0m"
	// LightBlue light blue text
	LightBlue = "\033[94m%s\033[0m"
)
