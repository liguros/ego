// Package misc are just short snippets of functions
package misc

// AddToSlice adds a string to a slice if it not exists.
func AddToSlice(sl []string, str string) []string {
	var exi = false

	for _, v := range sl {
		if v == str {
			exi = true
		} // if
	} // for

	if !exi {
		return append(sl, str)
	} // if

	return sl
} // AddToSlice ()

// RemoveFromSlice removes a string from a slice if it exists.
func RemoveFromSlice(sl []string, str string) (retSl []string) {
	for _, v := range sl {
		if v != str {
			retSl = append(retSl, v)
		} // if
	} // for

	return
} // RemoveFromSlice ()
