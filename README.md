ego - (repository) management tool for LiGurOS
==============================================

Version of ego written in go without kits support
-------------------------------------------------

Defined commands:

```
COMMANDS:
   news, n     Show release notes.
   profile, p  Manage profiles.
   query, q    Query package information.
   sync, s     Synchronize portage tree.
   help, h     Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --defaults, -d  Use default config files if they are not present (default: false)
   --help, -h      show help (default: false)
   --version, -v   print the version (default: false)
```

You could also `--help` or `-h` on any subcommand, like e.g. `ego news -h`

There is one command that needs special mention. In case you want to remove a mix-in from your profile, you can use the portage syntax with a dash as a prefix, although for ego to correctly handle the parameter, the mix-ins parameter needs to be escaped by a double dash, e.g.:

`ego profile mix-ins -- "-libressl"`

This also works with multiple mix-ins:

`ego profile mix-ins -- "-libressl openssl"`

