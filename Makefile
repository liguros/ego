# Go parameters
GOFMT      := gofmt
GOCMD      := go
GOBUILD    := GO111MODULE=on $(GOCMD) build -v -mod=vendor
GOCLEAN    := $(GOCMD) clean
GOINSTALL  := GO111MODULE=on $(GOCMD) install
GOTEST     := GO111MODULE=on $(GOCMD) test -v
GOVET      := GO111MODULE=on $(GOCMD) vet
GORACE     := $(GOCMD) race
GOTOOL     := GO111MODULE=on $(GOCMD) tool

SRCS       := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")
VERSION    := $(PROG_VERS)

ifeq ($(VERSION),)
	VERSION := testing
endif

ifeq ($(MDATE),)
	MDATE := $(shell date -u +%Y%m%d.%H%M%S)
endif

LDFL       := $(LDFLAGS) -X "main.Version=$(VERSION)" -X "main.Builddate=$(MDATE)"
EXECUTABLE := ego
export GO111MODULE=off

PACKAGES ?= $(shell find . -name "*.go" -print|grep -v /vendor/)

ifndef VERBOSE
.SILENT:
endif

.DEFAULT_GOAL := help

all: dep fmt build test coverage  ## Run all for development

dep: ## Get all the dependencies
		@echo "Getting dependencies"
		$(GOINSTALL) ./...
		$(GOINSTALL) github.com/fzipp/gocyclo/cmd/gocyclo@latest
		$(GOINSTALL) github.com/uudashr/gocognit/cmd/gocognit@latest
		$(GOINSTALL) github.com/gordonklaus/ineffassign@latest
		$(GOINSTALL) github.com/securego/gosec/v2/cmd/gosec@latest
		#$(GOINSTALL) github.com/client9/misspell/cmd/misspell@latest
		$(GOINSTALL) github.com/go-critic/go-critic/cmd/gocritic@latest

fmt: ## Format source code
		@echo "Formatting code"
		GO111MODULE=on $(GOFMT) -w -s $(PACKAGES)

test: ## Running tests
		@echo "Running tests"
		@echo "============="
#		@echo "Running staticcheck"
#		GO111MODULE=on staticcheck -checks all
		@echo "Running gocyclo"
		# ignore error code, we just want the messages
		-gocyclo -over 15 -ignore vendor .
		@echo "Running gocognit"
		# ignore error code, we just want the messages
		-gocognit -over 15 .|grep -v vendor
		#@echo "Running ineffassign"
		#GO111MODULE=on ineffassign ./...
#		@echo "Running misspell"
#		misspell * | grep -v vendor
		@echo "Running gosec"
		GO111MODULE=on gosec -no-fail --exclude G204 -tests ./...
		@echo "Running gocritic"
		GO111MODULE=on gocritic check -enableAll -disable='commentedOutCode'
		@echo "Running $(GOVET)"
		#$(GOVET) ./misc ./colors ./configuration
		$(GOVET) ./...
		@echo "Running $(GOTEST) race condition"
		#$(GOTEST) ./misc ./colors ./configuration
		$(GOTEST) ./...
		@echo "Running $(GOTEST) race memory sanity"
		#CC=clang $(GOTEST) -msan ./misc ./colors ./configuration
		CC=clang $(GOTEST) -msan ./.
		@echo "Running tests"
		#$(GOTEST) ./misc ./colors ./configuration
		$(GOTEST) ./. || exit 1;
		@echo "Running benchs"
		#$(GOTEST) -bench ./misc ./colors ./configuration
		$(GOTEST) -bench ./.
		@echo ""

build: ## Building development binary
		@echo "Running $(GOBUILD)"
		@echo "================"
		$(GOBUILD) -tags development -ldflags '$(LDFL)' -o ${EXECUTABLE} ./. ;
		@echo ""

coverage: ## Generating coverage for files
		@echo "Running coverage"
		@echo "================"
		$(GOTEST) ./... -coverpkg=./... -coverprofile coverage.cov
		$(GOTOOL) cover -func=coverage.cov

clean: ## Cleaning up
		@echo "Cleaning package"
		@echo "================"
		rm -f ${EXECUTABLE} coverage.cov

help:  ## Displaying help for build targets
		@echo "Available targets in this makefile:"
		@echo ""
		@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
