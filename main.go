// Ego - Liguros's personality tool
package main

import (
	"fmt"
	"os"

	"github.com/otiai10/copy"
	"github.com/urfave/cli/v2"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/liguros/ego/configuration"
)

// Having program name as constant to make it easier to change it
const (
	progName   = "ego" // main program
	parentFile = "/etc/portage/make.profile/parent"
)

// The values of Version and Builddate will be substituted by the build process with the git version and build date.
var (
	Version   = "undefined"
	Builddate = "undefined"
	defaults  bool
)

// verifyConfigs Copies some config files if they are not existing
func verifyConfigs() {
	// check liguros.conf
	err := copy.Copy("/etc/ego/liguros.conf.example", "/etc/portage/repos.conf/liguros.conf")
	misc.ShowError(err, "", "ErrPanic")
	// check parent file
	err = copy.Copy("/etc/ego/parent.example", parentFile)
	misc.ShowError(err, "", "ErrPanic")
} // verifyConfigs ()

func main() {
	// Modify version output
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("%s (version %s) build on %s\n", c.App.Name, c.App.Version, Builddate)
	}

	app := &cli.App{
		Name:                   progName,
		Usage:                  "Version of ego written in go without kits (use " + progName + " -h for usage)",
		UseShortOptionHandling: true,
		Version:                Version,
		Before: func(c *cli.Context) error {
			if defaults {
				verifyConfigs()
			} // if

			// Read config file and fill config values.
			configuration.ReadProgConfig("/etc/" + progName + "/" + progName + ".yml")
			return nil
		},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:        "defaults",
				Aliases:     []string{"d"},
				Usage:       "Use default config files if they are not present",
				Destination: &defaults,
			},
		},
		Action: func(c *cli.Context) error {
			if defaults {
				verifyConfigs()
			} else {
				fmt.Println(c.App.Usage)
			} // if

			return nil
		},
		Commands: []*cli.Command{
			{
				Name:    "news",
				Aliases: []string{"n"},
				Usage:   "Show release notes.",
				Subcommands: []*cli.Command{
					{
						Name:    "list",
						Aliases: []string{"l"},
						Usage:   "Show up to 10 latest release notes",
						Action: func(c *cli.Context) error {
							Releases(10)
							return nil
						},
					},
					{
						Name:    "release",
						Aliases: []string{"r"},
						Usage:   "Show latest release note",
						Action: func(c *cli.Context) error {
							Releases(1)
							return nil
						},
					},
				},
			},
			{
				Name:    "profile",
				Aliases: []string{"p"},
				Usage:   "Manage profiles.",
				Action:  actionProfileDefault,
				Subcommands: []*cli.Command{
					{
						Name:    "arch",
						Aliases: []string{"a"},
						Usage:   "Change your arch profile",
						Action:  actionProfile,
					},
					{
						Name:    "build",
						Aliases: []string{"b"},
						Usage:   "Change your build profile",
						Action:  actionProfile,
					},
					{
						Name:    "flavor",
						Aliases: []string{"f"},
						Usage:   "Change your flavor profile",
						Action:  actionProfile,
					},
					{
						Name:    "list",
						Aliases: []string{"l"},
						Usage:   "List given profiles (or all by default)",
						Action:  actionProfile,
					},
					{
						Name:    "mix-ins",
						Aliases: []string{"m"},
						Usage:   "Change your mix-ins profile",
						Action:  actionProfile,
					},
					{
						Name:    "subarch",
						Aliases: []string{"s"},
						Usage:   "Change your subarch profile",
						Action:  actionProfile,
					},
				},
			},
			{
				Name:    "query",
				Aliases: []string{"q"},
				Usage:   "Query package information.",
				Subcommands: []*cli.Command{
					{
						Name:    "origin",
						Aliases: []string{"o"},
						Usage:   "Show source repositority of package",
						Action:  actionQuery,
					},
				},
			},
			{
				Name:    "sync",
				Aliases: []string{"s"},
				Usage:   "Synchronize portage tree.",
				Action:  actionSync,
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "config-only",
						Aliases: []string{"c"},
						Usage:   "Update /etc/portage/repos.conf files only",
					},
					&cli.BoolFlag{
						Name:    "meta-repo-only",
						Aliases: []string{"m"},
						Usage:   "Sync only meta-repo.",
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
} // main ()

func actionProfile(c *cli.Context) error {
	// Verify config files if "defaults" flag is set
	if defaults {
		verifyConfigs()
	} // if

	//
	// TODO: Improve handling of -paramter
	//
	Profile(c.Command.Name, c.Args().First(), parentFile)
	return nil
} // actionProfile ()

func actionProfileDefault(c *cli.Context) error {
	// Verify config files if "defaults" flag is set
	if defaults {
		verifyConfigs()
	} // if

	Profile("", "", parentFile)
	return nil
} // actionProfileDefault ()

func actionQuery(c *cli.Context) error {
	if c.NArg() == 0 {
		fmt.Println("Usage: " + progName + " query origin <package>")
	} else {
		OriginList(c.Args().First())
	} // if

	return nil
} // actionQuery ()

func actionSync(c *cli.Context) error {
	if defaults {
		verifyConfigs()
	} // if

	return Segundo()
} // actionSync ()
